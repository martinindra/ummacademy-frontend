window.onscroll = () => {
  scrollFunction();
};

function scrollFunction() {
  if (
    document.body.scrollTop > 690 ||
    document.documentElement.scrollTop > 690
  ) {
    document.getElementById("navbar").classList.remove(`py-3`);
    document.getElementById("navbar").classList.add(`sticky-top`);
    document.getElementById("navbar").classList.add(`shadow`);
    document.getElementById("navbar").style.background = "#3AA1B1";
  } else {
    document.getElementById("navbar").style.background = "none";

    document.getElementById("navbar").classList.add(`py-3`);
    document.getElementById("navbar").classList.remove(`shadow`);
    document.getElementById("navbar").classList.remove(`sticky-top`);
  }
}

var imgSlider = (anything) => {
  document.querySelector(`.imgContent`).src = anything;
};

let kelasPK = `Kelas <br />
<span>Pra-Klinik & Klinik </span>`;
let kelasUk = `Kelas <br />
<span>UKMP2DG</span>`;

var textChange = (indicator) => {
  if (indicator == `1`) {
    document.querySelector(`#judul-program`).innerHTML = kelasPK;
    changeBorder(`#thumb-program1`, `#thumb-program2`);
    resetAnimationRight(`#judul-program`);
    resetAnimationRight(`#isi-program`);
    resetAnimationRight(`#button-program`);
  } else {
    changeBorder(`#thumb-program2`, `#thumb-program1`);
    resetAnimationRight(`#judul-program`);
    resetAnimationRight(`#isi-program`);
    resetAnimationRight(`#button-program`);
    document.querySelector(`#judul-program`).innerHTML = kelasUk;
  }
};

var changeBorder = (program1, program2) => {
  document.querySelector(program2).classList.remove(`border`);
  document.querySelector(program2).classList.remove(`border-5`);
  document.querySelector(program2).classList.remove(`border-danger`);
  document.querySelector(program1).classList.add(`border`);
  document.querySelector(program1).classList.add(`border-5`);
  document.querySelector(program1).classList.add(`border-danger`);
};

var resetAnimationRight = (element) => {
  document.querySelector(element).classList.remove(`fade-right`);
  document.querySelector(element).classList.remove(`fade-right`);
};
