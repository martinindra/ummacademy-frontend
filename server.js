const express = require("express");
const app = express();
require("dotenv").config();
const chalk = require("chalk");
const ejs = require("ejs");
const ejsLint = require("ejs-lint");
const router = require("./routes");
const port = process.env.PORT || 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.set("view engine", ejs);
app.set("views", __dirname + "/views");
app.use(express.static(__dirname + "/public"));
app.use(router);

app.listen(port, () =>
  console.log(chalk.bgGreenBright(chalk.black(`server is running on ${port}`)))
);
