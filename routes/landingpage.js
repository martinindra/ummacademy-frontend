const landingpage = require("../controller/landingpage");
const router = require(`express`).Router();

router.get(`/`, landingpage.landingpage);

module.exports = router;
