const router = require("express").Router();
const landingpage = require(`./landingpage`);

router.use(landingpage);

module.exports = router;
